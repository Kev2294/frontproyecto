import { Paciente } from "./paciente";

export class Signos {
    idSignos: number;
    id_paciente: Paciente;
    fecha: string; 
    temperatura: string;
    pulso: string;
    ritmo: string;
}