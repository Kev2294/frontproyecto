import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Signos } from 'src/app/_model/signos';
import { SignosService } from 'src/app/_service/signos.service';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Paciente } from 'src/app/_model/paciente';
import { PacienteService } from 'src/app/_service/paciente.service';
import * as moment from 'moment';
@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-vitales-edicion.component.html',
  styleUrls: ['./signos-vitales-edicion.component.css']
})
export class SignosVitalesEdicionComponent implements OnInit {

  //fecha: Date = new Date();

  pacientes$: Observable<Paciente[]>;

  form: FormGroup;
  id: number;
  edicion: boolean;
  idPacienteSeleccionado: number;
  pacienteSeleccionado: Paciente;
  maxFecha: Date = new Date();
  pacientes: any;
  pageType : string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private pacienteService : PacienteService,
    private signosService: SignosService
  ) { }
 
  ngOnInit(): void {
    this.pacientes$ = this.pacienteService.listar();
    console.log(JSON.stringify(this.pacientes$)+"lista pacientes");
    this.form = new FormGroup({
      'id': new FormControl(0),
      'paciente': new FormControl(''),
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo': new FormControl(''),
    });

    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      console.log(this.edicion+"edicion");
     
      this.initForm();
    });

    if(!this.edicion){
      this.pageType='new';
    }
    console.log(this.pageType+"pagetype");
  }

  private initForm() {
    if (this.edicion) {
      this.signosService.listarPorId(this.id).subscribe(data => {
        this.pageType='edit'
        console.log(JSON.stringify(data)+"data a modificar");
        console.log(this.pageType+"pagetype");
        this.pacientes=data.id_paciente;
        console.log(JSON.stringify(this.pacientes)+"pacientes");
        this.form = new FormGroup({
          'id': new FormControl(data.idSignos),
          'paciente': new FormControl(data.id_paciente),
          'fecha': new FormControl(data.fecha),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmo': new FormControl(data.ritmo),
        });
      });
    }
  }

  get f() {
    return this.form.controls;
  }

  operar() {
    if (this.form.invalid) { return; }

    let signos = new Signos();
    signos.idSignos = this.form.value['id'];
    signos.id_paciente = this.form.value['paciente'];
    signos.fecha = moment(this.form.value['fecha']).format('YYYY-MM-DDTHH:mm:ss');
    signos.temperatura = this.form.value['temperatura'];
    signos.pulso = this.form.value['pulso'];
    signos.ritmo = this.form.value['ritmo'];
  
    console.log(this.edicion);
    
    if (this.edicion) {
      //MODIFICAR
      //PRACTICA COMUN
      /*this.pacienteService.modificar(signos).subscribe(() => {
        this.pacienteService.listar().subscribe(data => {
          this.pacienteService.setPacienteCambio(data);
          this.pacienteService.setMensajeCambio('SE MODIFICO');
        });
      });*/
      //PRACTICA IDEAL      
      this.signosService.modificar(signos).pipe(switchMap(() => {
        return this.signosService.listar();
      }))
      .subscribe(data => {
        this.signosService.setSignosCambio(data);
        this.signosService.setMensajeCambio('SE MODIFICO');
      });

    } else {
      let paciente = new Paciente();
      paciente.idPaciente = this.idPacienteSeleccionado;
     
      this.pacienteService.listarPorId(paciente.idPaciente).subscribe(data => {
        this.pacienteSeleccionado = data;
        signos.id_paciente=this.pacienteSeleccionado;
        console.log(JSON.stringify(this.pacienteSeleccionado)+"paciente por id");
      });
    
    
      delete signos['idSignos'];
      console.log(JSON.stringify(signos)+"data a registrar");
      //REGISTRAR
      this.signosService.registrar(signos).subscribe(() => {
        this.signosService.listar().subscribe(data => {
          this.signosService.setSignosCambio(data);
          this.signosService.setMensajeCambio('SE REGISTRO');
        });
      });
    }

    this.router.navigate(['signos']);

  }

}
