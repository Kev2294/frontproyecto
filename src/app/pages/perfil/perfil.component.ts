import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { switchMap } from 'rxjs/operators';
import { ExamenService } from './../../_service/examen.service';
import { Examen } from './../../_model/examen';
import { Component, OnInit, ViewChild } from '@angular/core';
import { JwtHelperService } from "@auth0/angular-jwt";
import { MenuService } from 'src/app/_service/menu.service';
@Component({
  selector: 'app-examen',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {


  a:string ;
  b:string ;
  constructor(
    private menuService: MenuService,
   ) { }

  ngOnInit() {
   let id = sessionStorage.getItem('access_token');
   console.log(id+"token");
    const helper = new JwtHelperService();
    let decodedToken = helper.decodeToken(id);
    this.a= decodedToken.user_name;
    this.b= decodedToken.authorities;
    console.log(this.a+"usuario");
    console.log(JSON.stringify(this.b)+"auto");
    console.log(this.b+"auto");
  }


}
